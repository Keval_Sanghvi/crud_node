const express = require('express');
const dotenv = require('dotenv');
const morgan = require('morgan');
const bodyparser = require("body-parser");
const path = require('path');
const connectDB = require('./server/database/connection');

const app = new express();

app.use(morgan('tiny'));
app.use(bodyparser.urlencoded({ extended : true}));

app.set("view engine", "ejs");

app.use('/css', express.static(path.resolve(__dirname, "assets/css")));
app.use('/js', express.static(path.resolve(__dirname, "assets/js")));

dotenv.config({path: '.env'});
const PORT = process.env.PORT || 8000;

connectDB();

app.use('/', require('./server/routes/router'));

app.listen(PORT, () => {
    console.log(`Server running on PORT ${PORT}!`);
});